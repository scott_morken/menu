## Menu helper library

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation (Laravel)

The service provider and facade should automatically register.
If not, you can manually add `Smorken\Menu\ServiceProvider::class` to the
providers section of `config/app.php` and `'Menu' => Smorken\Menu\Facades\Menu::class`
to the aliases section of `config/app.php`.

Publish the files

```
php artisan vendor:publish --provider="Smorken\Menu\ServiceProvider"
```

This will provide a `config/menus.php`

Create your menus as desired.

Bootstrap 4 example - using Laravel via the Menu facade accessor

(`$controller` is simply view()->share('controller', get_called_class()) in the controller)
```html
@can('role-admin')
    <?php $menus = Menu::getMenusByKey('role-admin'); ?>
    @if ($menus)
        <li class="nav-item dropdown">
            <a id="navbarAdminDropdown"
               class="nav-link dropdown-toggle {{ Menu::isActiveChain($controller, $menus) ? 'text-primary' : null }}"
               href="#" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false" v-pre>
                Admin <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarAdminDropdown">
                @foreach($menus as $menu)
                    <?php $active = Menu::isActiveChain($controller, $menu); ?>
                    <?php $sub = $active && isset($menu['children']) ? $menu['children'] : null; ?>
                    <a class="dropdown-item {{ $active ? 'text-primary' : null }}"
                       href="{{ action($menu->action) }}">{{ $menu->name }}</a>
                @endforeach
            </div>
        </li>
    @endif
@endcan
@include('submenu')
```
Submenu
```html
<?php
$sub = isset($sub) ? $sub : null;
$next = null;
?>
@if ($sub)
    <nav class="sub-menu mb-1">
        <ul class="nav nav-pills">
            @foreach ($sub as $menu)
                <?php $active = Menu::isActiveChain($controller, $menu); ?>
                @if ($active && count($menu->children))
                    <?php $next = $menu->children; ?>
                @endif
                <li class="nav-item">
                    <a class="nav-link {{ $active ? 'active' : '' }}" href="{{ action($menu->action) }}">
                        {{ $menu->name }}
                    </a>
                </li>
            @endforeach
        </ul>
    </nav>
@endif
@if ($next)
    @include('submenu', ['sub' => $next])
@endif
```
