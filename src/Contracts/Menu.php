<?php

namespace Smorken\Menu\Contracts;

interface Menu
{
    public function addMenus(array $menus): void;

    public function getMenus(): array;

    public function getMenusByKey(string $key): array;

    public function getSubMenus(): array;

    public function isActiveChain($controller, array|Model $menu): bool;

    public function setMenus(array $menus): void;

    public function setSubMenus(array $submenus): void;
}
