<?php

namespace Smorken\Menu\Contracts;

/**
 * @property string $name
 * @property array $action
 * @property bool $visible
 * @property array $children
 *
 * @phpstan-require-extends \Smorken\Menu\Model\Menu
 */
interface Model
{
    public function __construct(array $menu = []);

    public function newInstance(array $menu): static;
}
