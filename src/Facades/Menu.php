<?php

namespace Smorken\Menu\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Menu
 *
 * @method static array getMenus()
 * @method static array getMenusByKey(string $key)
 * @method static array getSubMenus()
 * @method static bool isActiveChain($controller, $menu)
 * @method static void setMenus(array $menus)
 * @method static void setSubMenus(array $submenus)
 */
class Menu extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \Smorken\Menu\Contracts\Menu::class;
    }
}
