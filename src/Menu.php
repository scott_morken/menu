<?php

namespace Smorken\Menu;

use Smorken\Menu\Contracts\Model;

class Menu implements \Smorken\Menu\Contracts\Menu
{
    protected static array $submenus = [];

    protected array $menus = [];

    public function __construct(protected Model $model, array $menus)
    {
        $this->setMenus($menus);
    }

    public function addMenus(array $menus): void
    {
        $this->menus = array_merge_recursive($this->menus, $this->buildMenuArray($menus));
    }

    public function getMenus(): array
    {
        return $this->menus;
    }

    public function setMenus(array $menus): void
    {
        $this->menus = $this->buildMenuArray($menus);
    }

    public function getMenusByKey(string $key): array
    {
        if (array_key_exists($key, $this->menus) && is_array($this->menus[$key])) {
            return $this->menus[$key];
        }

        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function getSubMenus(): array
    {
        return static::$submenus;
    }

    public function isActiveChain($controller, array|Model $menu): bool
    {
        if (! $menu) {
            return false;
        }
        if ($menu instanceof Model) {
            if ($menu->action[0] === $controller) {
                return true;
            }

            return $this->isActiveChain($controller, $menu->children);
        }
        foreach ($menu as $k => $v) {
            if ($this->isActiveChain($controller, $v)) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function setSubMenus(array $submenus): void
    {
        static::$submenus = $submenus;
    }

    protected function buildMenuArray(array $menus = []): array
    {
        $arr = [];
        foreach ($menus as $k => $v) {
            if ($this->isMenuArray($v)) {
                $arr[$k] = $this->getModel()
                    ->newInstance($v);
            } else {
                $arr[$k] = $this->buildMenuArray($v);
            }
        }

        return $arr;
    }

    protected function getModel(): Model
    {
        return $this->model;
    }

    protected function isMenuArray(array $menu): bool
    {
        return isset($menu['name']);
    }
}
