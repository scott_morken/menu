<?php

namespace Smorken\Menu\Model;

use Smorken\Menu\Contracts\Model;

class Menu implements Model
{
    protected array $menu = [
        'name' => null,
        'visible' => true,
        'action' => [],
        'children' => [],
    ];

    public function __construct(array $menu = [])
    {
        if ($menu) {
            foreach ($menu as $k => $v) {
                $this->setAttribute($k, $v);
            }
        }
    }

    public function __get(string $key): mixed
    {
        return $this->getAttribute($key);
    }

    public function __set(string $key, mixed $value): void
    {
        $this->setAttribute($key, $value);
    }

    public function getAction(): array
    {
        return $this->fromAttributes('action', []);
    }

    public function getAttribute(string $key): mixed
    {
        $method = 'get'.ucfirst($key);
        if (method_exists($this, $method)) {
            return $this->$method();
        }

        return $this->fromAttributes($key);
    }

    public function getChildren(): array
    {
        return $this->fromAttributes('children', []);
    }

    public function newInstance(array $menu): static
    {
        return new static($menu);
    }

    public function setAction(array $action): void
    {
        $this->menu['action'] = $action;
    }

    public function setAttribute(string $key, mixed $value): void
    {
        $method = 'set'.ucfirst($key);
        if (method_exists($this, $method)) {
            $this->$method($value);
        } else {
            $this->menu[$key] = $value;
        }
    }

    public function setChildren(array $children = []): void
    {
        $items = [];
        foreach ($children as $child) {
            $items[] = $this->newInstance($child);
        }
        $this->menu['children'] = $items;
    }

    public function setName(string $name): void
    {
        $this->menu['name'] = $name;
    }

    protected function fromAttributes(string $key, mixed $default = null): mixed
    {
        if (array_key_exists($key, $this->menu)) {
            return $this->menu[$key];
        }

        return $default;
    }
}
