<?php

namespace Smorken\Menu;

use Smorken\Menu\Contracts\Menu;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
    }

    public function register(): void
    {
        $this->app->bind(Menu::class, function ($app) {
            $menus = $app['config']->get('menus', []);

            return new \Smorken\Menu\Menu(new \Smorken\Menu\Model\Menu, $menus);
        });
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'menus');
        // @phpstan-ignore function.notFound
        $this->publishes([$config => config_path('menus.php')], 'config');
    }
}
