<?php

namespace Tests\Smorken\Menu\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Menu\Menu;

class MenuTest extends TestCase
{
    public function testActiveChainFirstNoChildren(): void
    {
        $sut = $this->getSut();
        $this->assertTrue($sut->isActiveChain('AdminController', $sut->getMenusByKey('role-admin')[0]));
        $this->assertFalse($sut->isActiveChain('AdminController', $sut->getMenusByKey('role-admin')[1]));
    }

    public function testActiveChainMaxDepth(): void
    {
        $sut = $this->getSut();
        $this->assertFalse($sut->isActiveChain('AdminSubSubController', $sut->getMenusByKey('role-admin')[0]));
        $this->assertTrue($sut->isActiveChain('AdminSubSubController', $sut->getMenusByKey('role-admin')[1]));
        $this->assertFalse($sut->isActiveChain('AdminSubSubController',
            $sut->getMenusByKey('role-admin')[1]->children[0]));
        $this->assertTrue($sut->isActiveChain('AdminSubSubController',
            $sut->getMenusByKey('role-admin')[1]->children[1]));
        $this->assertTrue($sut->isActiveChain('AdminSubSubController',
            $sut->getMenusByKey('role-admin')[1]->children[1]->children[0]));
    }

    public function testActiveChainSecondFirstChild(): void
    {
        $sut = $this->getSut();
        $this->assertFalse($sut->isActiveChain('AdminSubController', $sut->getMenusByKey('role-admin')[0]));
        $this->assertTrue($sut->isActiveChain('AdminSubController', $sut->getMenusByKey('role-admin')[1]));
        $this->assertTrue($sut->isActiveChain('AdminSubController', $sut->getMenusByKey('role-admin')[1]->children[0]));
        $this->assertFalse($sut->isActiveChain('AdminSubController',
            $sut->getMenusByKey('role-admin')[1]->children[1]));
    }

    public function testActiveChainSingleFalse(): void
    {
        $sut = $this->getSut();
        $this->assertFalse($sut->isActiveChain('AdminController', $sut->getMenusByKey('role-manage')));
    }

    public function testActiveChainSingleTrue(): void
    {
        $sut = $this->getSut();
        $this->assertTrue($sut->isActiveChain('ManageController', $sut->getMenusByKey('role-manage')));
    }

    public function testAddMenusAppendsWhenNotSet(): void
    {
        $sut = $this->getSut();
        $sut->addMenus([
            'role-manage' => [
                [
                    'name' => 'Manage 2',
                    'action' => ['Manage2Controller', 'action'],
                    'children' => [],
                ],
            ],
            'role-admin' => [
                [
                    'name' => 'Menu Item Add',
                    'action' => ['AdminController', 'actionAdd'],
                    'children' => [
                        [
                            'name' => 'Menu Item Add 2',
                            'action' => ['AdminSubController', 'actionAdd2'],
                            'children' => [],
                        ],
                    ],
                ],
            ],
        ]);
        $this->assertCount(2, $sut->getMenusByKey('role-manage'));
        $this->assertCount(3, $sut->getMenusByKey('role-admin'));
    }

    public function testInGroup(): void
    {
        $sut = $this->getSut();
        $this->assertTrue($sut->isActiveChain('AdminSubController', $sut->getMenusByKey('role-admin')));
    }

    public function testNoMatch(): void
    {
        $sut = $this->getSut();
        $this->assertFalse($sut->isActiveChain('AdminFooController', $sut->getMenusByKey('role-admin')[0]));
        $this->assertFalse($sut->isActiveChain('AdminFooController', $sut->getMenusByKey('role-admin')[1]));
        $this->assertFalse($sut->isActiveChain('AdminFooController',
            $sut->getMenusByKey('role-admin')[1]->children[0]));
        $this->assertFalse($sut->isActiveChain('AdminFooController',
            $sut->getMenusByKey('role-admin')[1]->children[1]));
        $this->assertFalse($sut->isActiveChain('AdminFooController',
            $sut->getMenusByKey('role-admin')[1]->children[1]->children[0]));
    }

    public function testSetSubmenusMultipleInstancesIsTheSame(): void
    {
        $sut = $this->getSut();
        $sut->setSubMenus([
            'name' => 'Menu Item 3',
            'action' => ['AdminSubController', 'action'],
            'children' => [],
        ],
            [
                'name' => 'Menu Item 4',
                'action' => ['AdminSubElseController', 'action'],
                'children' => [
                    [
                        'name' => 'Menu Item 5',
                        'action' => ['AdminSubSubController', 'action'],
                        'children' => [],
                    ],
                ],
            ]);
        $newinst = $this->getSut();
        $this->assertNotEmpty($sut->getSubMenus());
        $this->assertEquals($newinst->getSubMenus(), $sut->getSubMenus());
    }

    protected function getMenus(): array
    {
        return [
            'guest' => [],
            'auth' => [],

            'role-manage' => [
                [
                    'name' => 'Manage Menu Item',
                    'action' => ['ManageController', 'action'],
                    'children' => [],
                ],
            ],
            'role-admin' => [
                [
                    'name' => 'Menu Item',
                    'action' => ['AdminController', 'action'],
                    'children' => [],
                ],
                [
                    'name' => 'Menu Item 2',
                    'action' => ['AdminElseController', 'action'],
                    'children' => [
                        [
                            'name' => 'Menu Item 3',
                            'action' => ['AdminSubController', 'action'],
                            'children' => [],
                        ],
                        [
                            'name' => 'Menu Item 4',
                            'action' => ['AdminSubElseController', 'action'],
                            'children' => [
                                [
                                    'name' => 'Menu Item 5',
                                    'action' => ['AdminSubSubController', 'action'],
                                    'children' => [],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function getSut(array $menus = []): \Smorken\Menu\Contracts\Menu
    {
        if (empty($menus)) {
            $menus = $this->getMenus();
        }

        return new Menu(new \Smorken\Menu\Model\Menu, $menus);
    }
}
